package com.zuitt.discussion.services;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.models.CourseEnrollment;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.CourseRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CourseServiceImplementation implements CourseService{
    @Autowired
    CourseRepository courseRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    JwtToken jwtToken;

    public void createPost(Course course) {

        Course newPost = new Course();
        newPost.setName(course.getName());
        newPost.setDescription(course.getDescription());
        newPost.setPrice(course.getPrice());
        newPost.setActive(true);

        courseRepository.save(newPost);

    }

    public Iterable<Course> getCourses(){
        return courseRepository.findAll();
    }

    public Optional<Course> getCourse(Integer courseId){
        if(courseRepository.findById(courseId).isEmpty()){
            return Optional.empty();
        }

        return Optional.of(courseRepository.findById(courseId).get());
    }

    public ResponseEntity deleteCourse(Integer id){

        if(courseRepository.findById(id).isEmpty()){
            return new ResponseEntity<>("Course with id "+id+" cannot be found.", HttpStatus.BAD_REQUEST);
        }
        courseRepository.deleteById(id);
        return new ResponseEntity<>("Course deleted successfully.", HttpStatus.OK);

    }

    public ResponseEntity updateCourse(Integer id, Course course){

        if(courseRepository.findById(id).isEmpty()){
            return new ResponseEntity<>("Course with id "+ id +" cannot be found.", HttpStatus.BAD_REQUEST);
        }
        Course courseForUpdate = courseRepository.findById(id).get();
        courseForUpdate.setName(course.getName());
        courseForUpdate.setDescription(course.getDescription());
        courseForUpdate.setPrice(course.getPrice());
        courseForUpdate.setActive(course.getActive());
        courseRepository.save(courseForUpdate);
        return new ResponseEntity<>("Course updated successfully!", HttpStatus.OK);

    }

    public ResponseEntity archive(Integer id){

        if(courseRepository.findById(id).isEmpty()){
            return new ResponseEntity<>("Course is not found!", HttpStatus.BAD_REQUEST);
        }
        Course course = courseRepository.findById(id).get();
        course.setActive(!course.getActive());
        courseRepository.save(course);
        return new ResponseEntity<>(course.getActive() ?
                "Course unarchived successfully!" :
                "Course archived successfully!", HttpStatus.OK);

    }

    public ResponseEntity<Object> getEnrollees(Integer courseId){

        if(courseRepository.findById(courseId).isEmpty()){
            return new ResponseEntity<>("Course not found!", HttpStatus.BAD_REQUEST);
        }
        Course course = courseRepository.findById(courseId).get();
        Iterable<User> users = course.getEnrollees().stream()
                .map(CourseEnrollment::getUserId)
                .map(User -> new User(User.getId(), User.getUsername())) //excludes the password from being returned
                .collect(Collectors.toList());
        return new ResponseEntity<>(users, HttpStatus.OK);

    }

}
