package com.zuitt.discussion.services;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.models.CourseEnrollment;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.CourseEnrollRepository;
import com.zuitt.discussion.repositories.CourseRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class CourseEnrollServImpl implements CourseEnrollService {
    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private JwtToken jwtToken;
    @Autowired
    private CourseEnrollRepository courseEnrollRepository;

    public ResponseEntity enroll(String stringToken, Integer courseId){
        User user = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        if(courseRepository.findById(courseId).isEmpty()){
            return new ResponseEntity<>("Course is not found!", HttpStatus.BAD_REQUEST);
        }

        Course course = courseRepository.findById(courseId).get();
        if(!course.getActive()){
            return new ResponseEntity<>("Course is archived, no longer available!", HttpStatus.FORBIDDEN);
        }
        CourseEnrollment courseEnrollment = new CourseEnrollment();
        courseEnrollment.setCourseId(course);
        courseEnrollment.setUserId(user);
        courseEnrollment.setDateTimeEnrolled(LocalDateTime.now());
        courseEnrollRepository.save(courseEnrollment);
        return new ResponseEntity<>("Course enrollment successful!", HttpStatus.OK);

    }

}
