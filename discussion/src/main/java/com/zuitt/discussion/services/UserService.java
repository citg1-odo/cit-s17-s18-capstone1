package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface UserService {
    Optional<User> findByUsername(String username);
    void createUser(User user);
    ResponseEntity getMyCourses(String stringToken);

}
