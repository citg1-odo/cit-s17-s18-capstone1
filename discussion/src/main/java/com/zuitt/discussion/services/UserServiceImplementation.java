package com.zuitt.discussion.services;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.models.CourseEnrollment;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImplementation implements UserService{
    @Autowired
    UserRepository userRepository;
    @Autowired
    JwtToken jwtToken;

    public Optional<User> findByUsername(String username){
        return Optional.ofNullable(userRepository.findByUsername(username));
    }
    public void createUser(User user){
        userRepository.save(user);
    }
    public ResponseEntity<Object> getMyCourses(String stringToken){
        User user = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Iterable<Course> courses = user.getEnrollments().stream()
                .map(CourseEnrollment::getCourseId)
                .collect(Collectors.toList());

        return new ResponseEntity<>(courses, HttpStatus.OK);
    }
}
