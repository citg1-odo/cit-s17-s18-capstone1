package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Course;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface CourseService {
    void createPost(Course course);
    Iterable<Course> getCourses();
    ResponseEntity deleteCourse(Integer id);
    ResponseEntity updateCourse(Integer id, Course course);
    Optional<Course> getCourse(Integer id);
    ResponseEntity archive(Integer id);
    ResponseEntity getEnrollees(Integer courseId);
}
