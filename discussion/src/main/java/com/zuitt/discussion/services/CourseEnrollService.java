package com.zuitt.discussion.services;


import org.springframework.http.ResponseEntity;

public interface CourseEnrollService {
    ResponseEntity enroll(String stringToken, Integer courseId);
}
