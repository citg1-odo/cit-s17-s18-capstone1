package com.zuitt.discussion.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "courses")
public class Course {
    @Id
    @GeneratedValue
    private Integer id;
    @Column
    private String name;
    @Column
    private String description;
    @Column
    private Double price;
    @Column(name = "is_active")
    private Boolean isActive;
    @OneToMany(mappedBy = "courseId")
    @JsonIgnore
    private Set<CourseEnrollment> enrollees;

    public Course() {
    }

    public Course(String name, String description, Double price, Boolean isActive) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.isActive = isActive;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Double getPrice() {
        return price;
    }

    public Boolean getActive() {
        return isActive;
    }

    public Set<CourseEnrollment> getEnrollees() {
        return enrollees;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }
}
