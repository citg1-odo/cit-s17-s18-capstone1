package com.zuitt.discussion.models;

import java.io.Serializable;

public class JwtResponse implements Serializable {
    private static final long serialVersionUID = 7705085851456516159L;
    private final String jwttoken;

    public JwtResponse(String jwttoken) {
        this.jwttoken = jwttoken;
    }

    public String getToken() {
        return jwttoken;
    }
}
