package com.zuitt.discussion.models;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "enroll")
public class CourseEnrollment {
    @Id
    @GeneratedValue
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "course_id", nullable = false)
    private Course courseId;
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User userId;
    @Column(name = "date_time_enrolled")
    private LocalDateTime dateTimeEnrolled;

    public CourseEnrollment() {
    }

    public CourseEnrollment(Course courseId, User userId, LocalDateTime dateTimeEnrolled) {
        this.courseId = courseId;
        this.userId = userId;
        this.dateTimeEnrolled = dateTimeEnrolled;
    }

    public Integer getId() {
        return id;
    }

    public Course getCourseId() {
        return courseId;
    }

    public User getUserId() {
        return userId;
    }

    public LocalDateTime getDateTimeEnrolled() {
        return dateTimeEnrolled;
    }

    public void setCourseId(Course courseId) {
        this.courseId = courseId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public void setDateTimeEnrolled(LocalDateTime dateTimeEnrolled) {
        this.dateTimeEnrolled = dateTimeEnrolled;
    }
}
