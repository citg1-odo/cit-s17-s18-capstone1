package com.zuitt.discussion.controllers;


import com.zuitt.discussion.services.CourseEnrollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class CourseEnrollController {
    @Autowired
    CourseEnrollService courseEnrollService;
    @PostMapping("/enroll/{courseid}")
    public ResponseEntity<Object> enroll(@RequestHeader(value = "Authorization") String stringToken,
                                         @PathVariable Integer courseid){
        return courseEnrollService.enroll(stringToken, courseid);
    }
}
