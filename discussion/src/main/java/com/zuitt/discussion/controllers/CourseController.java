package com.zuitt.discussion.controllers;

import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@CrossOrigin
public class CourseController {
    @Autowired
    CourseService courseService;

    @PostMapping("/courses")
    public ResponseEntity<Object> createPost(@RequestBody Course course){
        courseService.createPost(course);
        return new ResponseEntity<Object>("Course added successfully", HttpStatus.CREATED);
    }

    @GetMapping("/courses")
    public ResponseEntity<Object> getCourses(){
        return new ResponseEntity<Object>(courseService.getCourses(), HttpStatus.OK);
    }

    @GetMapping("/courses/{courseid}")
    public ResponseEntity<Object> getCourse(@PathVariable Integer courseid){
        return new ResponseEntity<>(courseService.getCourse(courseid), HttpStatus.OK);
    }

    @PutMapping("/courses/{courseid}")
    public ResponseEntity<Object> updateCourse(@PathVariable Integer courseid, @RequestBody Course course){
        return courseService.updateCourse(courseid, course);
    }

    @DeleteMapping("/courses/{courseid}")
    public ResponseEntity<Object> deleteCourse(@PathVariable Integer courseid){
        return courseService.deleteCourse(courseid);
    }

    @PutMapping("/courses/archive/{courseid}")
    public ResponseEntity<Object> archive(@PathVariable Integer courseid){
        return courseService.archive(courseid);
    }

    @GetMapping("courses/enrollees/{courseid}")
    public ResponseEntity<Object> getEnrollees(@PathVariable Integer courseid){
        return courseService.getEnrollees(courseid);
    }

}
